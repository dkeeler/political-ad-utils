package com.rentrak.politicaladspending.util

import org.opencompare.hac.dendrogram.Dendrogram
import org.opencompare.hac.dendrogram.MergeNode
import org.opencompare.hac.dendrogram.ObservationNode
import org.opencompare.hac.dendrogram.DendrogramNode
import org.apache.log4j.Logger

object DendrogramUtil {
  
  def linkDistances(dendrogram: Dendrogram): Seq[Double] = {
    collectDistances(dendrogram.getRoot)
  }
  
  private def collectDistances(node: DendrogramNode): List[Double] = {
    node match {
      case merged: MergeNode => {
        val leftDistances = collectDistances(merged.getLeft)
        val rightDistances = collectDistances(merged.getRight)
        
        merged.getDissimilarity :: leftDistances ::: rightDistances
      }
      case observation: ObservationNode => List.empty
    }
  }
  
  def cluster(dendrogram: Dendrogram, separatingDistance: Double): Set[Set[Int]] = {
    buildClusters(dendrogram.getRoot, separatingDistance)
  }
  
  private def buildClusters(node: DendrogramNode, separatingDistance: Double): Set[Set[Int]] = {
    node match {
      case merged: MergeNode => {
        if (merged.getDissimilarity < separatingDistance)
          Set(collectIds(merged))
        else {
          val rightClusters = buildClusters(merged.getLeft, separatingDistance)
          val leftClusters = buildClusters(merged.getRight, separatingDistance)
          
          rightClusters ++ leftClusters
        }
      }
      case observation: ObservationNode => Set(collectIds(observation))
    }
  }
  
  private def collectIds(node: DendrogramNode): Set[Int] = {
    node match {
      case merged: MergeNode => {
        val leftIds = collectIds(merged.getLeft)
        val rightIds = collectIds(merged.getRight)
        
        leftIds ++ rightIds
      }
      case observation: ObservationNode => Set(observation.getObservation)
    }
  }
}



















