package com.rentrak.politicaladspending.util

import java.io.File
import org.apache.commons.collections4.BidiMap
import org.apache.commons.collections4.bidimap.DualHashBidiMap
import scala.collection.JavaConverters._

/**
 * An extendable collection of documents.
 */
class DocumentCollection(initalDocs: Set[Document]) {
  private val docs: BidiMap[Int, Document] = {
    val docs = new DualHashBidiMap[Int, Document]
    initalDocs.zipWithIndex.map {
      case (doc, id) => docs.put(id, doc)
    }
    
    docs
  }
  
  lazy val maxPages = {
    docs.values.asScala.map {
      doc => doc.pages.map(_.length).max
    }.max
  }
  
  def getDoc(id: Int) = docs.get(id)
  
  def size = docs.size
}

case class Document(pages: Array[Page], docFile: File) {
  if (pages.length == 0) throw new IllegalArgumentException("Cannot create empty document.")
  
  lazy val terms: Array[String] = pages.map(_.terms).flatten
  lazy val length = terms.length
  
  def numPages = pages.length
}

case class Page(terms: Array[String]) {
  if (terms.length == 0) throw new IllegalArgumentException("Cannot create empty page.")
  
  def length = terms.length
}

case class DocumentCluster(docs: Set[Document], id: Int) {
  
  def foreach[U](f: Document => U) = docs.foreach(f)
}

