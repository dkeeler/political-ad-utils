package com.rentrak.politicaladspending.util

import java.lang.IllegalArgumentException
import java.io.File
import java.io.FileWriter
import java.io.FileFilter
import java.io.IOException
import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.util.PDFTextStripper
import org.opencompare.hac.experiment.Experiment
import scala.util.Random
import scala.io.Source
import scala.collection.mutable
import org.apache.log4j.Logger
import org.opencompare.hac.experiment.DissimilarityMeasure
import org.opencompare.hac.experiment.Experiment
import org.opencompare.hac.agglomeration.CentroidLinkage
import org.opencompare.hac.dendrogram.DendrogramBuilder
import org.opencompare.hac.dendrogram.Dendrogram
import org.opencompare.hac.HierarchicalAgglomerativeClusterer

object HierarchicalFormatClusterer {
  private val logger = Logger.getLogger(HierarchicalFormatClusterer.getClass)
  
  val minCollectionFrequency = 0.2
  
  def apply(docDir: File, numDocs: Int): HierarchicalFormatClusterer = {
    if (!docDir.isDirectory) throw new IllegalArgumentException(docDir.getAbsolutePath + " must be a dir.")
    
    val files = Random.shuffle(docDir.listFiles(pdfFilter).toList).slice(0, numDocs).toSeq
    HierarchicalFormatClusterer(files)
  }
    
  def apply(files: Seq[File]): HierarchicalFormatClusterer = {  
    val textStripper = new PDFTextStripper
    val rawDocs = 
      files.map {
          docFile => loadPdfDocument(docFile, textStripper)
      }.flatten
      
    val tokenToCount = collectionCount(rawDocs)
    val docs = rawDocs.map { 
      doc => normalize(doc, tokenToCount, rawDocs.size)
    }.flatten
    
    val docCollection = new DocumentCollection(docs.toSet)
    new HierarchicalFormatClusterer(docCollection)
  }
  
  private def loadPdfDocument(docFile: File, textStripper: PDFTextStripper): Option[Document] = {
    if (!pdfFilter.accept(docFile)) throw new IllegalArgumentException("Document must be a PDF.")
    
    try {
      val pdfDoc = PDDocument.load(docFile)
      val pageOptions =
        for (pageNumber <- 1 to pdfDoc.getNumberOfPages) yield {
          textStripper.setStartPage(pageNumber)
          textStripper.setEndPage(pageNumber)
          val pageText = textStripper.getText(pdfDoc)
          
          val tokens = tokenize(pageText)
          if (tokens.size > 0) {
            val page = Page(tokens)
            Some(page)
          } else {
            None
          }
        }
      pdfDoc.close
      
      val pages = pageOptions.flatten.toArray
      if (pages.size > 0)
        Some(Document(pages, docFile))
      else 
        None
    } catch {
      case e: IOException => None
    }
  }

  private val pdfFilter = new FileFilter {
    def accept(pathname: File): Boolean = pathname.getName.endsWith(".pdf")
  }
  
  private def tokenize(rawText: String): Array[String] = {
    rawText.split("\\s+").map(_.trim).filterNot(_.isEmpty)
  }
  
  private def normalize(
      doc: Document, 
      tokenToCount: Map[String, Int], 
      collectionSize: Int): Option[Document] = {
    
    val commonTermsOnly: Option[Document] = removeUncommonTerms(doc, tokenToCount, minCollectionFrequency * collectionSize)

    if (commonTermsOnly.isDefined) 
      removeLikelyFieldValues(commonTermsOnly.get)
    else 
      None
  }
  
  private def removeUncommonTerms(doc: Document, tokenToCount: Map[String, Int], threshold: Double): Option[Document] = {
    val pageOptions = 
      for (page <- doc.pages) yield {
        val commonTerms = page.terms.filter(term => tokenToCount(term) > threshold)
        if (commonTerms.length > 0)
          Some(Page(commonTerms))
        else
          None
      }

    val pages = pageOptions.flatten
    if (pages.length > 0)
      Some(Document(pages, doc.docFile))
    else 
      None
  }
  
  private def removeLikelyFieldValues(doc: Document): Option[Document] = {
    val pageOptions = 
      for (page <- doc.pages) yield {
        val normedTerms = page.terms.map {
          term => term.trim.filterNot(_.isDigit)
        }.filterNot {
          _.isEmpty
        }.filter {
          term => Character.isUpperCase(term.charAt(0))
        }
        
        if (normedTerms.length > 0)
          Some(new Page(normedTerms))
        else
          None
      }

    val pages = pageOptions.flatten
    if (pages.length > 0)
      Some(Document(pages, doc.docFile))
    else 
      None
  }
  
  def collectionCount(docs: Iterable[Document]): Map[String, Int] = {
    val tokenToCount = mutable.Map[String, Int]()
    
    for (
      doc <- docs;
      token <- doc.terms.distinct) {
      
      if (tokenToCount.contains(token)) 
        tokenToCount.put(token, tokenToCount(token) + 1)
      else
        tokenToCount.put(token, 1)
    }
    
    tokenToCount.toMap
  }
}

class HierarchicalFormatClusterer(private val docs: DocumentCollection) {
  import HierarchicalFormatClusterer.logger
  
  def cluster(separatingDistance: Double): Set[DocumentCluster] = {
    val dendrogram = hacCluster
    val clusteredDocIds = DendrogramUtil.cluster(dendrogram, separatingDistance)
    
    clusteredDocIds.toList.sortBy(_.size).reverse.zipWithIndex.map {
      case (cluster, clusterId) => 
        val clusteredDocs = cluster.map(docs.getDoc(_))
        DocumentCluster(clusteredDocs, clusterId)
    }.toSet
  }
    
  private def hacCluster: Dendrogram = { 
    val start = System.currentTimeMillis
    
    val experiment = new Experiment {
      def getNumberOfObservations = docs.size
    }
    val distanceMeasure = new FormatDissimilarityMeasure(docs)
    val linkMethod = new CentroidLinkage
    val dendrogramBuilder = new DendrogramBuilder(experiment.getNumberOfObservations)
    val clusterer = new HierarchicalAgglomerativeClusterer(experiment, distanceMeasure, linkMethod)
    
    clusterer.cluster(dendrogramBuilder)
    
    val end = System.currentTimeMillis
    logger.info("Number of pairs evaluated: " + distanceMeasure.numEvaluations)
    logger.info("Cluster time: " + (end - start)/1000.0 + " seconds.")
    
    dendrogramBuilder.getDendrogram
  }
}
















