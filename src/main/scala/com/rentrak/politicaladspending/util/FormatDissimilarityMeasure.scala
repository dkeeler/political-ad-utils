package com.rentrak.politicaladspending.util

import org.opencompare.hac.experiment.DissimilarityMeasure
import org.opencompare.hac.experiment.Experiment
import org.apache.log4j.Logger

object FormatDissimilarityMeasure {
  def editDistance(terms1: Array[String], terms2: Array[String], distanceTable: Array[Array[Int]]): Int = {
    initDistTable(distanceTable)
    
    for (i <- 1 to terms1.length;
        j <- 1 to terms2.length) {
        
        if (terms1(i - 1) == terms2(j - 1)) 
          distanceTable(i)(j) = distanceTable(i - 1)(j - 1)
        else 
          distanceTable(i)(j) = Seq(distanceTable(i - 1)(j) + 1, distanceTable(i)(j - 1) + 1, distanceTable(i - 1)(j - 1) + 1).min
    }
    
    distanceTable(terms1.length)(terms2.length)
  }
  
  private def initDistTable(distanceTable: Array[Array[Int]]) {
    for (
      i <- 0 until distanceTable.length;
      j <- 0 until distanceTable(i).length) {
      
      distanceTable(i)(j) =
        if (j == 0) i
        else if (i == 0) j
        else 0
    }
  }
}

class FormatDissimilarityMeasure(docs: DocumentCollection) extends DissimilarityMeasure {
  private val logger = Logger.getLogger(FormatDissimilarityMeasure.getClass)
  
  private val distanceTable: Array[Array[Int]] = Array.ofDim[Int](docs.maxPages + 1, docs.maxPages + 1)
  
  private var count = 0
  def numEvaluations: Int = count
    
  def computeDissimilarity(experiment: Experiment, observation1: Int, observation2: Int): Double = {    
    val lowerIndex = Math.min(observation1, observation2)
    val upperIndex = Math.max(observation1, observation2)
    
    val dist = distance(observation1, observation2)
    
    count += 1
    if (count % 10000 == 0) logger.info(count + " pair comparisons.")
    
    dist
  }
  
  private def distance(observation1: Int, observation2: Int): Double = {
    val doc1 = docs.getDoc(observation1)
    val doc2 = docs.getDoc(observation2)
    
    normedEditDistance(doc1, doc2)
  }
  
  private def normedEditDistance(doc1: Document, doc2: Document): Double = {
    val (morePages, lessPages) = 
      if (doc1.numPages >= doc2.numPages) (doc1, doc2)
      else (doc2, doc1)
      
    val distanceAndLengths =   
      for (page <- lessPages.pages) yield {
        
        morePages.pages.map {
          otherPage => 
            val length = Math.max(page.length, otherPage.length)
            (editDistance(page, otherPage), length)
        }.minBy(_._1)
      }

    val totalDistance = distanceAndLengths.map(_._1).sum
    val totalLength = distanceAndLengths.map(_._2).sum
    totalDistance/totalLength
  }
  
  private def editDistance(page1: Page, page2: Page): Double = {
    val length = Math.max(page1.length, page2.length)
    FormatDissimilarityMeasure.editDistance(page1.terms, page2.terms, distanceTable).toDouble
  }
}






