package com.rentrak.politicaladspending.util

import java.io.File
import java.io.FileWriter
import java.io.FileFilter
import java.io.IOException
import scala.io.Source
import scala.collection.mutable

import org.apache.commons.io.FileUtils
import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.util.PDFTextStripper
import org.apache.log4j.Logger

object GroupByFormat {
  private val logger = Logger.getLogger(GroupByFormat.getClass)
  
  private val separatingDistance = 0.2
  
  def main(args: Array[String]) {
    val options = GroupByFormatArgsParser.parse(args)
    writeNotes(options)
    
    val clusterer = HierarchicalFormatClusterer(options.docDir, options.numDocs)
    val clusters = clusterer.cluster(separatingDistance)
    
    val clustersFile = new File(options.outputDir, "clusters.tsv")
    writeClusters(clusters, clustersFile)
    
    if (options.copyFiles)
      copyFiles(clusters, options.outputDir)
  }
  
  private def writeClusters(clusters: Set[DocumentCluster], outputFile: File) {
    val writer = new FileWriter(outputFile)
    
    for (cluster <- clusters; doc <- cluster) {
        writer.write(cluster.id + "\t" + doc.docFile.getName + "\n")
    }
    writer.close
  }
  
  private def copyFiles(clusters: Set[DocumentCluster], outputDir: File) {
    for (cluster <- clusters; doc <- cluster) {
      val srcFile = doc.docFile
      val destDir = new File(outputDir, cluster.id.toString)
      val destFile = new File(destDir, srcFile.getName)
      
      if (!destDir.exists) destDir.mkdir
      FileUtils.copyFile(srcFile, destFile)
    }
  }
  
  private def writeNotes(options: GroupByFormatOptions) {
    val notesFile = new File(options.outputDir, "notes.txt")
    val notesWriter = new FileWriter(notesFile)
    notesWriter.write(options + "\n")
    notesWriter.close
  }
}

case class GroupByFormatOptions(
    docDir: File, 
    outputDir: File, 
    separatingDistance: Double, 
    minCollectionFrequency: Double, 
    numDocs: Int,
    copyFiles: Boolean) {
  
  override def toString = {
    val builder = new StringBuilder
    builder.append("docDir: " + docDir + "\n")
    builder.append("outputDir: " + outputDir + "\n")
    builder.append("separatingDistance: " + separatingDistance + "\n")
    builder.append("minCollectionFrequency: " + minCollectionFrequency + "\n")
    builder.append("numDoc: " + numDocs + "\n")
    
    builder.toString
  }
}
    
object GroupByFormatArgsParser {
  val docDir = "--docDir"
  val outputDir = "--outputDir"
  val separatingDistance = "--separatingDistance"
  val minCollectionFrequency = "--minCollectionFrequency"
  val numDoc = "--numDocs"
  val copyFiles = "--copyFiles"
  
  def parse(args: Array[String]): GroupByFormatOptions = {
    val labelToValue = parse(args.toList)
    
    GroupByFormatOptions(
        new File(labelToValue(docDir)),
        new File(labelToValue(outputDir)),
        labelToValue(separatingDistance).toDouble,
        labelToValue(minCollectionFrequency).toDouble,
        labelToValue(numDoc).toInt,
        labelToValue(copyFiles).toBoolean)
  }
  
  private def parse(args: List[String]): Map[String, String] = {
    args match {
      case GroupByFormatArgsParser.docDir :: value :: tail => parse(tail) ++ Map(docDir -> value)
      case GroupByFormatArgsParser.outputDir :: value :: tail => parse(tail) ++ Map(outputDir -> value)
      case GroupByFormatArgsParser.separatingDistance :: value :: tail => parse(tail) ++ Map(separatingDistance -> value)
      case GroupByFormatArgsParser.minCollectionFrequency :: value :: tail => parse(tail) ++ Map(minCollectionFrequency -> value)
      case GroupByFormatArgsParser.numDoc :: value :: tail => parse(tail) ++ Map(numDoc -> value)
      case GroupByFormatArgsParser.copyFiles :: tail => parse(tail) ++ Map(copyFiles -> "true")
      case List() => Map.empty
      case _ => throw new IllegalArgumentException("Unable to parse arguments.")
    }
  }
}













